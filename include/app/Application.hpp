#pragma once


#include <memory>
#include <vector>
#include <string>

namespace app {

class Application {
public:
    static std::shared_ptr<Application> getInstance();
    void pushArgs(int argc, char ** argv);
    
    void run();
    const std::vector<std::string>& args() const;
    
    ~Application();
    
protected:
    struct _pImpl;
    std::unique_ptr<_pImpl> _impl;
    static std::shared_ptr<Application> _inst;
    Application ();
    
    std::vector<std::string> _args;
    
}; 



}



