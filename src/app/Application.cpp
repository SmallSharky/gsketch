#include <app/Application.hpp>

#include <gtkmm/application.h>
#include "MainWindow.hpp"

#include <stdexcept>

namespace app {

struct Application::_pImpl {
    Glib::RefPtr<Gtk::Application> gtkApp;
};
    
std::shared_ptr<Application> Application::_inst = nullptr;




std::shared_ptr<Application> Application::getInstance(){
    if(!_inst){
        std::shared_ptr<Application> app(new Application);
        _inst = std::move(app);
    }
    return _inst;
}



Application::Application()
{
    _impl = std::make_unique<_pImpl>();
    if(!_impl){
        throw std::runtime_error("app::Application() cannot create pImpl");
    }
    _impl->gtkApp = Gtk::Application::create("org.sharky.gsketch");
    
    if(!_impl->gtkApp){
        throw std::runtime_error("app::Application() cannot create Gtk::Application");
    }
}

void Application::pushArgs(int argc, char ** argv)
{
    _args.reserve(_args.size() + std::max(0, argc));
    for(int i = 0; i<argc; ++i){
        _args.push_back(argv[i]);
    }
}


void Application::run() {
    MainWindow win;
    _impl->gtkApp->run(win);
}

Application::~Application() {
    
}


const std::vector<std::string>& Application::args () const {
    return _args;
}

}
