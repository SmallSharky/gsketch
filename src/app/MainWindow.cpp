
#include "MainWindow.hpp"
#include <iostream>
#include <giomm/settings.h>
#include <gtkmm/headerbar.h>
#include <gtkmm/button.h>

struct MainWindow::_pImpl {
    Gtk::HeaderBar header;
    Gtk::Button    btn_left_panel;
    
    
};


MainWindow::MainWindow():
Gtk::ApplicationWindow()
{
    _impl = std::make_unique<_pImpl>();
    _impl->header.set_title("GSketch");
    _impl->header.set_subtitle("startup stub");
    _impl->header.set_show_close_button(true);
    _impl->btn_left_panel.set_label("|<-");
    _impl->header.add(_impl->btn_left_panel);
    
    _impl->header.show_all();
    set_title("GSketch");
    Gdk::Geometry geom = {
        .min_width   = 640,
        .min_height  = 480,
    };
    set_geometry_hints(*this,geom,Gdk::HINT_MIN_SIZE);
    this->set_titlebar(_impl->header);
}
}



MainWindow::~MainWindow()
{
    
}
