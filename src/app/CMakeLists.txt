
add_executable(
gsketch 

main.cpp
MainWindow.cpp
MainWindow.hpp
Application.cpp
)

install(TARGETS gsketch RUNTIME DESTINATION bin)


target_link_directories(gsketch PUBLIC ${GTKMM_LIBRARY_DIRS})

target_include_directories(gsketch PUBLIC ${GTKMM_INCLUDE_DIRS})

target_link_libraries(gsketch ${GTKMM_LIBRARIES} toolset)
