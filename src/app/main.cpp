#include <gtkmm.h>
#include <stdexcept>
#include <iostream>
#include <app/Application.hpp>


int main(int argc, char *argv[])
{
    int ret = 0;
    auto app = app::Application::getInstance();
    app->pushArgs(argc, argv);
    try{
    app->run();
    } catch (std::exception e) {
        std::cout<<"ERR: "<<e.what()<<"\n";
        ret = -1;
    }
    return 0;
//   auto app = Gtk::Application::create("org.gtkmm.examples.base");
// 
//   Gtk::Window window;
//   window.set_default_size(200, 200);
// 
//   return app->run(window);
}
