#pragma once

#include <memory>
#include <gtkmm/applicationwindow.h>



class MainWindow: public Gtk::ApplicationWindow {
public:
    
    MainWindow();
    virtual ~MainWindow();
    
private:
    struct _pImpl;
    std::unique_ptr<_pImpl> _impl;
};
